This material is for developers that are interested in creating applications for the Glow platform.

If you just want to use your smart meter with existing apps, then go to https://www.glowmarkt.com/ or to purchase hardware https://shop.glowmarkt.com/

Use the presentation material and Postman collection to get access to Glowmarkt data.

